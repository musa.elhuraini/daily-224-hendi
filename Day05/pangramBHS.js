function pangramBHS(str){
    var lib = ""; //unurk mencari librari a-z
    for (let ch = 97; ch <= 122; ch++){
        lib += String.fromCharCode(ch);       
    }

    //menyisir setiap indexs string dengan indexs library
    for(let i = 0; i < str.length; i++){
        var chr = str.substr(i, 1);
        var idx = lib.indexOf(chr);
        if(idx > -1){
            lib = lib.replace(chr.toLowerCase(), '');
            if (lib.length == 0) {
                break;
            }
        }
    }

    if(lib.length == 0){
        console.log("PANGRAM")
    }else{
        console.log("NOT PANGRAM")
    }
}