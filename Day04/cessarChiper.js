function ceasarChiper(str) {
    var strArr = str.split();
    for (let i = 0; i < strArr.length; i++) {
        if (strArr[i].charCode(0) >= 97 && strArr[i].charCode(0) <= 120) {
            strArr[i] = String.fromCharCode(strArr[i].charCode(0) + 2);
        }
        else if (strArr[i].charCode(0) >= 121 && strArr[i].charCode(0) <= 122) {
            strArr[i] = String.fromCharCode(strArr[i].charCode(0) - 24);
        }
        else if (strArr[i].charCode(0) >= 65 && strArr[i].charCode(0) <=88 ) {
            strArr[i] = String.fromCharCode(strArr[i].charCode(0) + 2);
        }
        else if (strArr[i].charCode(0) >= 89 && strArr[i].charCode(0) <= 90) {
            strArr[i] = String.fromCharCode(strArr[i].charCode(0) - 24);
        }
    }
    var arr2d = array2Dimensi(2,2);
    arr2d = [
        ["String", str],
        ["Result", strArr.join("")]
    ];
    printHTML(arr2d, false);
}