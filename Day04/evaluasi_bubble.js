function bubble(arr){
    let tmp;
    while(true){
        var status = true;
        for(let j = 0; j < arr.length-1; j++){
            if(arr[j] > arr[j+1]){
                status = false;
                tmp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = tmp;
            }
        }
        if(status){
            break;
        }
    }
    console.table(arr);
    printHTML(new Array(arr));
}