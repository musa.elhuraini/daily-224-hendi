function catur(q, n) {
    //Q : B2, N : C4
    //Q : 21, N : 02
   // var col = ["A", "B", "C", "D"];
    var qPos = cPos(q);
    var nPos = cPos(n);

    var board = array2Dimensi(4, 4);
    board[qPos.row][qPos.col] = "Q";
    board[nPos.row][nPos.col] = "N"

    var safe = true;
    if (qPos.row == nPos.row ||
        qPos.col == nPos.col) {
        safe = false;
    }

    for (let row = 0; row < board.length; row++) {
        for (let col = 0; col < board[row].length; col++) {
            if (row + qPos.col == col + qPos.row || row + col == qPos.row + qPos.col) {
                if (board[row][col] == "N") {
                    safe = false;
                }else{
                    board[row][col] = "X"
                }
            }
        }
    }
    console.table(board);
    console.log(safe);
}

function cPos(q) {
    var col = ["A", "B", "C", "D"];
    var qPos = {
        row : 4 - q.substr(1, 1),
        col : col.indexOf(q.substr(0, 1))
    }
    return qPos;
}
