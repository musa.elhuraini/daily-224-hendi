function plusmin(arr){
    var inp = arr.split(" ");
    var positifC = 0;
    var negatifC = 0;
    var zerofC = 0;
    for(let i=0; i < inp.length ; i++){
        if(Math.sign(parseInt(inp[i]))== 1){
            positifC++;
        }else if(Math.sign(parseInt(inp[i])) == -1){
            negatifC++
        }else if (Math.sign(parseInt(inp[i]))== 0){
            zerofC++
        }
    }
    console.log(`Positive : ${(positifC/inp.length).toFixed(6)}`);
    console.log(`Negatif : ${(negatifC/inp.length).toFixed(6)}`);
    console.log(`Zero : ${(zerofC/inp.length).toFixed(6)}`);
}