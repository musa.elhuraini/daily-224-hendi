function diagonal(arr) {
    var diag1 = 0;// kiri-kanan
    var diag2 = 0;//kanan-kiri
    //baris
    for (let row = 0; row < arr.length; row++) {
        //kolom
        for (let col = 0; col < arr[row].length; col++) {
            if (row == col) {
                diag1 += arr[row][col];
            }
            if (row + col == arr.length - 1) {
                diag2 += arr[row][col];
            }
        }
    }
    console.log(`Result diag1 : ${diag1}, diag2 : ${diag2}`);
    console.log(`Diff ${diag1} and ${diag2} = ${Math.abs(diag1 - diag2)}`);

}