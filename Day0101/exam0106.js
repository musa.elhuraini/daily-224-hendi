function exam0106(n) {
    var arr = [];
    for (let i = 0; i < n; i++) {
        if ((i + 1) % 3 == 0) {
            arr.push("*");
        } else {
            arr.push((i * 4) + 1);
        }
    }
    console.table(new Array(arr));
}