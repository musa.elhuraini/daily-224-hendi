function exam0105(n) {
    var arr = [];
    var nilai = 0;
    for (let i = 0; i < n; i++) {
        if ((i + 1) % 3 == 0) {
            arr.push("*");
        } else {
            arr.push((nilai * 4) + 1);
            nilai ++;
        }
    }
    console.table(new Array(arr));
}