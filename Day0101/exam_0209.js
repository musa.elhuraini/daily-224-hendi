function exam0209(kalimat) {
    var arrWords = kalimat.split(" ");
    // console.log(arrWords);
    var result = "";
    arrWords.forEach(word => {
        for (let i = 0; i < word.length; i++) {
            //console.log(word.length - 1);
            if (i == 0 || i == word.length - 1) {
                result += word.substr(i, 1);
            }
            else {
                result += "*";
            }
        }
        result += " ";
    });
    
    console.log(result);

}