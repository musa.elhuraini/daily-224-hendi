function exam0201(n1, n2) {
    var arr = new Array(2); // mendefinisikan array yang akan di pakai
    arr[0] = new Array(n1); // menentukan indek dari array nya, dengan memberikan keterangan // [0]
    arr[1] = new Array(n1); // menentukan indek dari array nya, dengan memberikan keterangan // [1]
    for (let col = 0; col < n1; col++) {
        arr[0][col] = col; // meberikan isi dalam kolom
        arr[1][col] = Math.pow(n2, col); // menentukan pangkat dalam baris
    }
    console.table(arr);
}