function exam0202(n1, n2) {
    var arr = new Array(2);
    arr[0] = new Array(n1);
    arr[1] = new Array(n1);
    for (let col = 0; col < n1; col++) {
        arr[0][col] = col;
        if ((col + 1) % 3 == 0) {
            arr[1][col] = Math.pow(n2,col) * -1;
        } else {
            arr[1][col] = Math.pow(n2, col);
        }
    }
    console.table(arr);
}