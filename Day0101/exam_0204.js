function exam0204(n1, n2) {
    var arr = new Array(2);
    arr[0] = new Array(n1);
    arr[1] = new Array(n1);
    var nilai = 1;
    var selang = n2;
    for (let col = 0; col < n1; col++) {
        arr[0][col] = col;
        if ((col + 1) % 2 == 0) {
            arr[1][col] = n2;
            n2 = n2 + selang;
        } else {
            arr[1][col] = nilai;
            nilai++;
        }
    }
    console.table(arr);
}