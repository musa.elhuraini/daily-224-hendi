function findMissingNumber(params) {
    var param0 = params[0];
    var param1 = params[1];

    for (let i = 0; i < param0.length; i++) {
        var idx = param1.indexOf(param0[i])
        if (idx != -1) {
            param1.splice(idx, 1);
        }
    }
    console.log(param1);
}