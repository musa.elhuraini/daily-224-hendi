function timeConvert(params) {
    var timerArr = params.split(":");
    var valid = true;
    var arr = new Array();
    if (timerArr[0] == 00 && timerArr[2].substr(2, 2) == "PM") {
        valid = false;
        console.log('invalid Time Format');
    } else if (timerArr[0] >= 13 || timerArr[1] >= 60 || timerArr[2] >= 60) {
        valid = false;
        console.log('invalid time format');
    } else if (timerArr[2].substr(2, 2) == "PM") {
        timerArr[0] = parseInt(timerArr[0]) + 12;
    }
    if (valid) {
        arr[0] = `${timerArr[0]} : ${timerArr[1]} : ${timerArr[2].substr(0, 2)}`;
    } else {
        arr[0] = `invalid data `;
    }
    console.log(arr[0]);
}