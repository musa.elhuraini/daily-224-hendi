function PlusMinus(params) {
    var Plus = 0;
    var minus = 0;
    var nol = 0;
    var input = params.split(",");
    console.log(input);
    for (let i = 0; i < input.length; i++) {
        if (Math.sign(parseInt(input[i]))== 1) {
            Plus++;
        }
        if(Math.sign(parseInt(input[i]))== 0){
            nol++;
        }
        if (Math.sign(parseInt(input[i]))== -1) {
            minus++;
        }
    }
    console.log(`Panjang Inputan : ${input.length}\nPlus : ${Plus} dibagi ${input.length}\nZero : ${nol} dibagi ${input.length}\nMinus: ${minus} dibagi ${input.length} `);
    console.log(`Hasil = `);
    console.log(`Plus  : ${(Plus/input.length).toFixed(6)}`);
    console.log(`Zero  : ${(nol/input.length).toFixed(6)}`);
    console.log(`Minus : ${(minus/input.length).toFixed(6)}`);
}