function diagonalDifrence(params) {
    var diag1 = 0;
    var diag2 = 0;
    //mencari baris
    for (let row = 0; row < params.length; row++) {
        //Kolom
        for (let col = 0; col < params[row].length; col++) {
            if (row == col) {
                diag1 += params[row][col];
            }
            if (row + col == params.length-1) {
                diag2 += params[row][col];
            }
        }
    }
    console.log(`hasil \n diag 1 : ${diag1} \n diag 2 : ${diag2} \n Difference ${Math.abs(diag1-diag2)}`);
}