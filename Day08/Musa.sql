--3
select MHS."Kode_Mahasiswa",
	MHS"Nama_Mahasiswa",
	MHS."Kode_Jurusan",
	jur."Nama_Jurusan",
	AGM."Deskripsi"
from "Mahasiswa"AS MHS
	JOIN "jurusan" AS jur on
		MHS."Kode_Jurusan" = jur."Kode_Jurusan"
	JOIN "Agama" AS AGM on
		MHS."Kode_Agama" = AGM."Kode_Agama"
where MHS."Kode_Mahasiswa" = 'M001'

--4
select * from "Mahasiswa" As MHS
	Join "jurusan" As jur on
		MHS."Kode_Jurusan" = jur."Kode_Jurusan"
	where "Status_Jurusan" = 'Non Aktif'
--5
select distinct MHS.* 
from "Mahasiswa" AS MHS
	JOIN "Nilai" AS "NIL" ON
		MHS."Kode_Mahasiswa" = "NIL"."Kode_Mahasiswa"
	JOIN "Ujian" AS Uji ON
		"NIL"."Kode_Ujian" = Uji."Kode_Ujian"
where "Status_Ujian" = 'Aktif' AND 
	"Nilai" > 80

--6
select to_char(
	AVG("Nilai"), '99D99'
) AS "AVG"  
	FROM "Nilai"
---------------
select Distinct MHS.*
	from"Mahasiswa" as MHS
		JOIN "Nilai" as "Nil" ON
			MHS."Kode_Mahasiswa" = "Nil"."Kode_Mahasiswa"
	where "Nilai" >= (Select AVG("Nilai") From "Nilai")
	
--7
select Distinct MHS.*
	from"Mahasiswa" as MHS
		JOIN "Nilai" as "Nil" ON
			MHS."Kode_Mahasiswa" = "Nil"."Kode_Mahasiswa"
	where "Nilai" <= (Select AVG("Nilai") From "Nilai")
	
--8
ALTER TABLE "jurusan"
ADD COLUMN "Biaya" Numeric(10,2)

Kode_Jurusan	Biaya
J001			450000
J002			475000
J003			455000
J004			470000
J005			450000

Update "jurusan" Set "Biaya" = 450000
	where "Kode_Jurusan" = 'J001'
	----------------------------------------------
Update "jurusan" Set "Biaya" = 475000
	where "Kode_Jurusan" = 'J002'
	----------------------------------------------
Update "jurusan" Set "Biaya" = 455000
	where "Kode_Jurusan" = 'J003'
	----------------------------------------------
Update "jurusan" Set "Biaya" = 470000
	where "Kode_Jurusan" = 'J004'
	-----------------------------------------------
Update "jurusan" Set "Biaya" = 450000
	where "Kode_Jurusan" = 'J005'
	-----------------------------------------------
Select * from "jurusan"

Alter Table public. "jurusan"
	Alter Column "Biaya" Set NOT NULL
	
--cara hapus 
Delete form "jurusan" where "Id" = ?
--

9---
select sum("Biaya") As "JML"
	from "jurusan"
	where "Status_Jurusan" = 'Aktif'
	
10--
select * 
	from "Dosen" AS DOS
		Join "jurusan" AS JUR ON
			DOS."Kode_Jurusan" = JUR."Kode_Jurusan"
Order by "Biaya" DESC
----Specifik
select "Nama_Dosen", "Biaya" 
	from "Dosen" AS DOS
		Join "jurusan" AS JUR ON
			DOS."Kode_Jurusan" = JUR."Kode_Jurusan"
Order by "Biaya" DESC

--11
Select * from
	"jurusan"
where "Nama_Jurusan" Like ('%Sistem%')

--12
select MHS."Kode_Mahasiswa", MHS."Nama_Mahasiswa", Count(MHS."Kode_Mahasiswa") As JML 
	from "Mahasiswa" AS MHS
		JOIN "Nilai" AS "NIL" ON
			MHS."Kode_Mahasiswa"  = "NIL"."Kode_Mahasiswa"
	--where <spesifik menyebutkan nama kolom nya>
Group BY MHS."Kode_Mahasiswa", MHS."Nama_Mahasiswa"
having Count(MHS."Kode_Mahasiswa") > 1

--13
-- Kode_Mahasiswa	Nama_Mahasiswa	Nama_Jurusan		Agama	Nama_Dosen						Status_Jurusan	Deskripsi
-- M001				Budi Gunawan	Teknik Informatika	Islam	Prof. Dr. Retno Wahyuningsih	Aktif			Honorer
select 
	MHS."Kode_Mahasiswa",
	MHS."Nama_Mahasiswa",
	JUR."Nama_Jurusan",
	AGM."Deskripsi" AS "Agama",
	DOS."Nama_Dosen",
	JUR."Status_Jurusan",
	TYD."Deskripsi"
	
	from "Mahasiswa" AS MHS
		JOIN "jurusan" AS JUR ON
			MHS."Kode_Jurusan" = JUR."Kode_Jurusan"
		JOIN "Agama" AS AGM ON
			MHS."Kode_Agama" = AGM."Kode_Agama"
		JOIN "Dosen" AS DOS ON
			JUR."Kode_Jurusan" = DOS."Kode_Jurusan"
		JOIN "Type_Dosen" As TYD ON
			DOS."Kode_Type_Dosen" = TYD."Kode_Type_Dosen"
	where MHS."Kode_Mahasiswa" = 'M001'


--14
select * From "View_Mahasiswa" AS VMHS
	JOIN "Nilai" AS "NIL" ON
		VMHS."Kode_Mahasiswa" = "NIL"."Kode_Mahasiswa"

--15
select mhs.*, "nil"."Nilai"
from "Mahasiswa" as MHS
	Left Join "Nilai" as "nil" on
		MHS."Kode_Mahasiswa" = "nil"."Kode_Mahasiswa"


--Ekstra
update "jurusan" set "Status_Jurusan" = 'Non Aktif'
	where "Kode_Jurusan" in ('J003','J005')
