var studentList = [
    { id: 1, first_name: 'Budi', last_name: 'Kurniawan' },
    { id: 2, first_name: 'Iwan', last_name: 'Sudirman' }
]

module.exports = exports = (server) => {
    server.get('/api/students', (req, res, next) => {
        res.send(200, {
            success: true,
            data: studentList
        })
    });
    server.get('/api/student/:id', (req, res, next) => { // fungsi untuk melihat data
        var id = req.params.id;
        var student = studentList.find(s => s.id == id);
        if (student) {
            res.send(200, {
                success: true,
                data: student
            })
        } else {
            res.send(400, {
                success: false,
                data: student
            })
        }
    });
    server.post('/api/student', (req, res, next) => { //fungsi untuk menambah data
        const { first_name, last_name } = req.body;
        var newId = Math.max.apply(Math, studentList.map(s => { return s.id })) + 1;
        var student = {
            id: newId,
            first_name: first_name,
            last_name: last_name,
        }
        studentList.push(student);
        res.send(200, {
            success: true,
            data: student
        })
    });
    server.put('/api/student/:id', (req, res, next) => { //fungsi untuk mengupdate data
        var id = req.params.id;
        const { first_name, last_name } = req.body
        var student = studentList.find(s => s.id == id);
        if (student) {
            if (first_name) student.first_name = first_name;
            if (last_name) student.last_name = last_name;
            res.send(200, {
                success: true,
                data: student
            });
        } else {
            res.send(400, {
                success: false,
                error: 'Muridnya hilang'
            })
        }
    });
    server.del('/api/student/:id', (req, res, next) => {
        var id = req.params.id;
        var idx = studentList.findIndex(s => s.id == id);
        if (idx > -1) {
            studentList.splice(idx, 1);
            res.send(200, {
                success: true,
                message: 'has ben delete'
            });
        } else {
            res.send(400, {
                success: false,
                message: 'student not found'
            });
        }
    });
}