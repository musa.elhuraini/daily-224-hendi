const restify = require('restify');

const server = restify.createServer({
    name: 'XPOS',
    version: '1.0.0',
});

server.use(restify.plugins.bodyParser(
    {
        mapParams: false
    }
));

server.get('/', (req, res, next) => {
    var body =
        `<html>
            <body>
                <h3>Welcome to XPOS API Web</h3>
            </body>
        </htm>`;
    res.writeHead(200, {
        'Content-Length' : Buffer.byteLength(body),
        'Content-Type' : 'text/html'
    });
    res.write(body);
    res.end();
});
require('./services/student_service')(server);

server.listen(3000, () =>{
    console.log(`${new Date()}${server.name} listen at ${server.url}`);
});