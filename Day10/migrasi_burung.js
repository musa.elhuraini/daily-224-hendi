function migrasi(arr) {
   // arr.sort(function (a, b) { return a - b})
       const numMap = {};
       var maxNum = 0;
       var maxChar = 0;
       for (var num of arr) { 
           if (numMap[num]) {
               numMap[num]++;
           } else { 
               numMap[num] = 1;
           }
       }
       for (var num in numMap) { 
           if (numMap[num] > maxNum) { 
               maxNum = numMap[num];
               maxChar = num;
           }
       }
       return maxChar; 
   }