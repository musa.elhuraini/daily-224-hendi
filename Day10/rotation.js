function rotation(params, banyakLoop) {
    var temp = 0;
    for (let i = 0; i < banyakLoop; i++) {
        temp = params[0];
        for (let j = 0; j < params.length - 1; j++) {
            params[j] = params[j + 1];
        }
        params[params.length - 1] = temp;
        console.log(params);
    }
}

function rotation1(arr, lengthLoop) {
    var temp = 0;
    for (let i = 0; i < lengthLoop; i++) {
        temp = arr[0];
        for (let j = 0; j < arr.length - 1; j++) {
            arr[j] = arr[j + 1];
        }
        arr[arr.length - 1] = temp;
        console.log(arr);
    }
}

function rotasi(arr, n) {
    var result = [];
    for (let k = 0; k < n; k++) {
        var row = [];
        for (let j = 0; j < arr.length; j++) {
            if (j == arr.length - 1) {
                row.push(arr[0]);
            }
            else {
                row.push(arr[j + 1]);
            }
        }
        arr = row;
        result.push(row);
    }
    console.table(result);
}