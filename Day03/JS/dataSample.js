var dataSample ={
    deret3x3 : [
        [11,2,4],
        [4,5,6],
        [10,8,-12]
    ],
    deret4x4 : [
        [1,2,3, 5],
        [4,5,6, 6],
        [7,8,9, 9]
    ],
    gemstone : [
        ["abcdde"],
        ["baccd"],
        ["eeabg"]
    ]
}