function timeConvert_musa(val) {
    var timeArr = val.split(":");
    var valid = true;
    var arr = new Array();
    if (timeArr[0] == 00 && timeArr[2].substr(2, 2) == "PM") {
        valid = false;
        console.log('invalid Time Format');
    } else if (timeArr[0] >= 13 || timeArr[1] >= 60 || timeArr[2] >= 60) {
        valid = false;
        console.log('invalid time format');
    } else if (timeArr[2].substr(2, 2) == "PM") {
        timeArr[0] = parseInt(timeArr[0]) + 12;
    }
    if (valid) {
        arr[0] = `${timeArr[0]} : ${timeArr[1]} : ${timeArr[2].substr(0, 2)}`;
        // console.log(`${timeArr[0]} : ${timeArr[1]} : ${timeArr[2].substr(0, 2)}`);
    } else {
        arr[0] = `invalid data tabel`;
    }
    printHML(new Array(arr));
}